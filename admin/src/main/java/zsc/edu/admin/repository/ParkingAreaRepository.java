package zsc.edu.admin.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import zsc.edu.admin.model.ParkingArea;

public interface ParkingAreaRepository extends JpaRepository<ParkingArea, Integer> {
}
