package zsc.edu.admin.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import zsc.edu.admin.model.Admin;

import java.util.Optional;

public interface AdminRepository extends JpaRepository<Admin,Integer> {

    Optional<Admin> findByUsername(String username);
}
