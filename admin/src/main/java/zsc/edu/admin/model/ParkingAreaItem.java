package zsc.edu.admin.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;

@Entity
@Data
@EqualsAndHashCode(callSuper = true)
public class ParkingAreaItem extends BaseEntity<Integer>{
}
