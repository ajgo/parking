package zsc.edu.admin.security.jwt;

import org.springframework.http.HttpHeaders;

import java.time.Duration;

public class SecurityConstants {
    private SecurityConstants() {
    }

    public static final String AUTH_LOGIN_URL = "/auth/login";

    public static final String ROLE_CLAIMS = "rol";

    public static final long EXPIRATION = Duration.ofDays(3).toMillis();

    public static final long EXPIRATION_REMEMBER = Duration.ofDays(7).toMillis();

    public static final String JWT_SECRET_KEY = "mySecret_not_too_simple_bal...";

    public static final String TOKEN_HEADER = HttpHeaders.AUTHORIZATION;

    public static final String TOKEN_PREFIX = "Bearer";

    public static final String TOKEN_TYPE = "JWT";
}
