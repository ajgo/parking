package zsc.edu.admin.security;

import org.modelmapper.ModelMapper;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import zsc.edu.admin.model.Admin;
import zsc.edu.admin.repository.AdminRepository;

import java.util.Optional;

@Service
public class DefaultUserDetailsService implements UserDetailsService {

    private final AdminRepository adminRepository;
    private final ModelMapper modelMapper;

    public DefaultUserDetailsService(AdminRepository adminRepository, ModelMapper modelMapper) {
        this.adminRepository = adminRepository;
        this.modelMapper = modelMapper;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<SecurityUser> securityUser = adminRepository
                .findByUsername(username)
                .map(this::convertToSecurityUser);
        return securityUser.orElseThrow(() -> new UsernameNotFoundException("用户名不存在"));
    }

    private SecurityUser convertToSecurityUser(Admin admin) {
        return modelMapper.map(admin, SecurityUser.class);
    }
}
