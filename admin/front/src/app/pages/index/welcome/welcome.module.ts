import {NgModule} from '@angular/core';

import {WelcomeRoutingModule} from './welcome-routing.module';
import {WelcomeComponent} from './welcome.component';
import {AComponent} from './a.component';
import {BComponent} from './b.component';

@NgModule({
  imports: [WelcomeRoutingModule],
  declarations: [WelcomeComponent, AComponent, BComponent],
  exports: [WelcomeComponent]
})
export class WelcomeModule {
}
