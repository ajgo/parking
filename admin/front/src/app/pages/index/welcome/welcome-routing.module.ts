import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {WelcomeComponent} from './welcome.component';
import {AComponent} from './a.component';
import {BComponent} from './b.component';

const routes: Routes = [
  {
    path: '', component: WelcomeComponent,
    children: [
      {path: 'a', component: AComponent},
      {path: 'b', component: BComponent},
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WelcomeRoutingModule {
}
