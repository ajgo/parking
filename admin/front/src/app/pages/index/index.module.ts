import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {IndexRoutingModule} from './index.routing.module';
import {IndexComponent} from './index.component';
import {NzLayoutModule} from 'ng-zorro-antd/layout';
import {NzMenuModule} from 'ng-zorro-antd/menu';
import {NzFormModule} from 'ng-zorro-antd/form';
import {NzInputModule} from 'ng-zorro-antd/input';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {IconsProviderModule} from '../../icons-provider.module';


@NgModule({
  declarations: [IndexComponent],
  imports: [
    IndexRoutingModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NzLayoutModule,
    NzMenuModule,
    NzFormModule,
    NzInputModule,
    IconsProviderModule,
  ],
  exports: [IndexComponent]
})
export class IndexModule {
}
