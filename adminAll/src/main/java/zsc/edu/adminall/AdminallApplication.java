package zsc.edu.adminall;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AdminallApplication {

    public static void main(String[] args) {
        SpringApplication.run(AdminallApplication.class, args);
    }

}
